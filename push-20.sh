#!/usr/bin/env bash
if [ -n "$1" ]; then
    cp -r . "../fork-$1"
    cd ../fork-$1
    git checkout "branch-$1"
fi
start_time=$SECONDS
i=0
while [ $i -ne 20 ]
do
    i=$(($i+1))
    echo "$i" >> ./log.txt
    git commit -am "Commit $i/20"
    git push origin -q
done
elapsed=$(( SECONDS - start_time ))
echo "$1 completed in $elapsed seconds"
if [ -n "$1" ]; then
    rm -rf ../fork-$1
fi
